use std::collections::VecDeque;

#[derive(Debug,Clone, Copy,PartialEq, Eq, PartialOrd, Ord)]
pub enum Instruction {
    NOP,                // LITERALY DO NOTHING
    ADD(u16),           // +
    SUB(u16),           // -
    MVL(u16),           // <
    MVR(u16),           // >
    JMP(Option<i16>),   // [
    JNZ(Option<i16>),   // ]
    PUT,                // .
    GET,                // ,
}



#[derive(Debug)]
pub struct Interperter {
    pub source_code:&'static str,
    pub byte_code: VecDeque<Instruction>,
}

impl Interperter {
    pub fn new(source_code: &'static str) -> Self {
        let mut byte_code: VecDeque<Instruction> = VecDeque::new();


        // converting the source code to bytecode
        for chr in source_code.chars() {
            if "+-<>[],.".contains(chr) {
                byte_code.push_back(
                    match chr {
                        '+' => Instruction::ADD(1),
                        '-' => Instruction::SUB(1),
                        '<' => Instruction::MVL(1),
                        '>' => Instruction::MVR(1),
                        '[' => Instruction::JMP(None),
                        ']' => Instruction::JNZ(None),
                        '.' => Instruction::PUT,
                        ',' => Instruction::GET,
                        _ => Instruction::NOP,      // we should never recive this
                    }
                )
            }
        }
        // packing instructions
        let mut tmp_vec:VecDeque<Instruction> = VecDeque::new();
        while !byte_code.is_empty() {
            let mut tmp_n:usize = 1;
            let tmp_i:Instruction = byte_code.pop_front().unwrap();
            while let Some(&ins) = byte_code.front() {
                if ins != tmp_i || [Instruction::GET, Instruction::PUT, Instruction::JMP(None), Instruction::JNZ(None)].contains(&tmp_i) {
                    break;
                }
                tmp_n += 1;
                _ = byte_code.pop_front().unwrap();
                
            }
            tmp_vec.push_back(
                match tmp_i {
                    Instruction::ADD(_) => Instruction::ADD(tmp_n as u16),
                    Instruction::SUB(_) => Instruction::SUB(tmp_n as u16),
                    Instruction::MVL(_) => Instruction::MVL(tmp_n as u16),
                    Instruction::MVR(_) => Instruction::MVR(tmp_n as u16),
                    _ => tmp_i
                }
            )
        }
        byte_code = tmp_vec.clone();
        //TODO: add support for loops within loops
        // now dealing with the jmps
        for i in 0..byte_code.len() {
            if let Some(&ins) = byte_code.get(i) {
                if ins == Instruction::JMP(None) {
                    let mut n:i16 = 0;
                    while byte_code[i + n as usize] != Instruction::JNZ(None) {
                        n+=1;
                    }
                    byte_code[i] = Instruction::JMP(Some(n));
                    byte_code[i + n as usize] = Instruction::JNZ(Some(1-n));
                }

            }
        }


        Self {source_code, byte_code}
    }
    pub fn decrypt(byte_code: VecDeque<Instruction>) -> String {
        let mut source_code:String = String::new();
        for ins in byte_code.iter() {
            let mut to_push:String = String::new();
            match ins {
                Instruction::NOP => (),
                Instruction::PUT => to_push.push('.'),
                Instruction::GET => to_push.push(','),
                Instruction::JMP(_) => to_push.push('['),
                Instruction::JNZ(_) => to_push.push(']'),
                Instruction::ADD(n) => to_push.push_str("+".repeat(*n as usize).as_str()),
                Instruction::SUB(n) => to_push.push_str("-".repeat(*n as usize).as_str()),
                Instruction::MVR(n) => to_push.push_str(">".repeat(*n as usize).as_str()),
                Instruction::MVL(n) => to_push.push_str("<".repeat(*n as usize).as_str()),

            }
            source_code += &to_push;
        }
        source_code
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let bfi = Interperter::new("++++--  >>>>>> [] ,. 43 ");
        println!("bfi - {:?}",bfi);
    }

    #[test]
    fn hello_world() {
        use Instruction::*;
        let bfi = Interperter::new(">++++++++[<+++++++++>-]<.>++++[<+++++++>-]<+.+++++++..+++.>>++++++[<+++++++>-]<++.------------.>++++++[<+++++++++>-]<+.<.+++.------.--------.>>>++++[<++++++++>-]<+.");
        assert_eq!(bfi.byte_code, VecDeque::from(
            vec![
MVR(1), ADD(8), JMP(Some(5)), MVL(1), ADD(9), MVR(1), SUB(1), JNZ(Some(-4)), MVL(1), PUT, MVR(1), ADD(4), JMP(Some(5)), MVL(1), ADD(7), MVR(1), SUB(1), JNZ(Some(-4)), MVL(1), ADD(1), PUT, ADD(7), PUT, PUT, ADD(3), PUT, MVR(2), ADD(6), JMP(Some(5)), MVL(1), ADD(7), MVR(1), SUB(1), JNZ(Some(-4)), MVL(1), ADD(2), PUT, SUB(12), PUT, MVR(1), ADD(6), JMP(Some(5)), MVL(1), ADD(9), MVR(1), SUB(1), JNZ(Some(-4)), MVL(1),ADD(1), PUT, MVL(1), PUT, ADD(3), PUT, SUB(6), PUT, SUB(8), PUT, MVR(3), ADD(4), JMP(Some(5)), MVL(1), ADD(8), MVR(1), SUB(1), JNZ(Some(-4)), MVL(1), ADD(1), PUT

            ])
       )
    }
    #[test] 
    fn packing() {
        use Instruction::*;
        let bfi = Interperter::new("--,,,+++");
        assert_eq!(bfi.byte_code, VecDeque::from(vec![SUB(2),GET, GET, GET,ADD(3)]));
    }
    #[test]
    fn decrypt() {
        let bfi = Interperter::new("++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>>++.>+.+++++++..+++.<<++.>+++++++++++++++.>.+++.------.--------.<<+.<.");
        assert_eq!(bfi.source_code, Interperter::decrypt(bfi.byte_code));
    }

}
